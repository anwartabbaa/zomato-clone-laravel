<?php

use Illuminate\Support\Facades\File;

if(!function_exists('photoPath')){
    function photoPath($image_name)
    {
        return public_path('images/products/'.$image_name);
    }
}

if (!function_exists('addPhotoFile'))
{

    /**
     * @param $request
     * @return string|null
     */
     function addPhotoFile($request){
        if ($image = $request->file('logo')) {
            $destinationPath = 'image/';
            $profileImage = $image->getATime() . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            return $destinationPath . $profileImage;
        }
         if ($image = $request->file('photo')) {
             $destinationPath = 'image/';
             $profileImage = $image->getATime() . "." . $image->getClientOriginalExtension();
             $image->move($destinationPath, $profileImage);
             return $destinationPath . $profileImage;
         }
        return  null;
    }
}

if (!function_exists('deletePhotoFile')){
    /**
     * @param String $photoUrl
     * @return bool|null
     */
     function deletePhotoFile(String $photoUrl){
        if(File::exists(public_path($photoUrl))){
            return File::delete(public_path($photoUrl));
        }
        return null;
    }
}
