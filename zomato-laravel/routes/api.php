<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\DriverController;
use App\Http\Controllers\DriverOrdersController;
use App\Http\Controllers\OrdersController;
use App\Http\Controllers\RestaurantAuthController;
use App\Http\Controllers\RestaurantController;
use App\Http\Controllers\RestaurantInfoController;
use App\Http\Controllers\OrderItemController;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\UserInfoController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\CartItemsController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/adddriver', [DriverController::class, 'addDriver']);
Route::delete('/delete_driver/{id}', [DriverController::class, 'delete']);
Route::put('update_driver/{id}', [DriverController::class, 'updateDriver']);
Route::get('/getdrivers', [DriverController::class, 'index']);
Route::get('/getdriver/{id}', [DriverController::class, 'getDriver']);


/////////////////////////////User Info /////////////////////////////////////////////////


Route::post('/adduserinfo', [UserInfoController::class, 'addUserInfo']);
Route::delete('/delet_userinfo/{id}', [UserInfoController::class, 'delete']);
Route::put('update_userinfo/{id}', [UserInfoController::class, 'updateUserInfo']);
Route::get('/getuserinfo', [UserInfoController::class, 'index']);
Route::get('/getuserinfo/{id}', [UserInfoController::class, 'getUserInfo']);



/////////////////////////////////Driver Order ///////////////////////////////////////////
Route::post('/adddriverorder', [DriverOrdersController::class, 'adddriver_order']);
Route::delete('/delete_driver_order/{id}', [DriverOrdersController::class, 'delete']);
Route::put('update_driver_order/{id}', [DriverOrdersController::class, 'updateDriverOrder']);
Route::get('/getdriver_orders', [DriverOrdersController::class, 'index']);


///////////////////////////////////Orders //////////////////////////////////////////////

Route::post('/addorder', [OrdersController::class, 'addOrder']);
Route::delete('/delete_order/{id}', [OrdersController::class, 'delete']);
Route::put('update_order/{id}', [OrdersController::class, 'updateOrder']);
Route::get('/getorders', [OrdersController::class, 'index']);
Route::get('/getorder/{id}', [OrdersController::class, 'getOrder']);



////////////////////////////////Driver Authentication /////////////////////////////////////
Route::group(['prefix' => 'drivers'], function() {
Route::post('/register', [DriverController::class,  'register']);
Route::post('/login', [DriverController::class, 'login']);
Route::group(['middleware' => ['jwt.driver']], function () {
        Route::post('/logout', [DriverController::class,  'logout']);
    });
});

/*---------user affect user------------*/
Route::group(['prefix'=>'user'],function (){
    Route::delete('/delete/{id}',[UserController::class,'destroy']);
    Route::get('/list',[UserController::class,'index']);
    Route::get('/list/{id}',[UserController::class,'getUser']);
});
/*-----------user routes------------*/
Route::group(['prefix'=>'user'],function (){
    Route::group(['middleware'=>['jwt.user']],function(){
        Route::post('/logout',[UserController::class,'logout']);

    });
    Route::post('/login',[UserController::class,'login']);
    Route::post('/register',[UserController::class,'register']);
});

//Route::resource('user', (string)[UserController::class]);

Route::group(['prefix' => 'admin'], function() {
    Route::post('/login', [AdminController::class,'login']);
    Route::post('/addadmin', [AdminController::class,'addAdmin']);
    Route::put('/update/{id}', [AdminController::class,'update']);
    Route::delete('/delete/{id}', [AdminController::class,'delete']);
    Route::get('/getadmins', [AdminController::class,'index']);
    Route::get('/getadmin/{id}', [AdminController::class,'getAdmin']);
    Route::group(['middleware' => ['jwt.admin']], function () {
        Route::post('/logout', [AdminController::class,'logout']);

    });
});


Route::group(['prefix' => 'item'], function() {
    Route::post('/additem', [ItemController::class,'addItem']);
    Route::put('/update/{id}', [ItemController::class,'update']);
    Route::delete('/delete/{id}', [ItemController::class,'delete']);
    Route::get('/getitem', [ItemController::class,'index']);
    Route::get('/getitem/{id}', [ItemController::class,'getItem']);
});


Route::group(['prefix' => 'orderitem'], function() {
    Route::post('/addorderitem', [OrderItemController::class,'addOrderItem']);
    Route::put('/update/{id}', [OrderItemController::class,'update']);
    Route::delete('/delete/{id}', [OrderItemController::class,'delete']);
    Route::get('/getorderitem', [OrderItemController::class,'index']);
});



//Route::apiResource('/restaurantInfoaurant',\App\Http\Controllers\RestaurantController::class);
//Route::apiResource('/restaurantInfo',\App\Http\Controllers\RestaurantInfoController::class);

Route::group(['prefix'=>'restaurant'],function(){

    Route::post('/login',[RestaurantAuthController::class,'login']);
    Route::post('/register',[RestaurantAuthController::class,'register']);
    Route::apiResource('/details',RestaurantController::class);
    Route::apiResource('/profile',RestaurantInfoController::class);
    Route::apiResource('/category',CategoryController::class);
    Route::group(['middleware'=>['jwt.restaurant']],function(){

        Route::post('/logout',[RestaurantAuthController::class,'logout']);
    });

});
Route::group(['prefix'=>'cartItem'],function(){
    Route::get('/getitems/{id}/{restaurant_id}',[CartItemsController::class,'index']);
    Route::post('/addtocart',[CartItemsController::class,'store']);
    Route::post('/checkout',[CartItemsController::class,'checkout']);
    Route::delete('/deleteitem/{id}',[CartItemsController::class,'destroy']);
    Route::put('/update/{id}',[CartItemsController::class,'update']);
});
Route::put('/update/{id}',[UserController::class,'update']);
Route::get('CategoriesByResturant/{id}',[CategoryController::class,'getCategoriesByResturant']);





