<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddCartItemRequest;
use App\Http\Requests\CheckoutRequest;
use App\Http\Requests\UpdateCartItemQuantity;
use App\Models\CartItem;
use App\Models\Item;
use App\Models\Order;
use App\Models\OrderItem;

class CartItemsController extends Controller
{
    /**
     * Get all cart items for specific user and restaurant
     *
     * @param $id
     * @param $resturantId
     * @return \Illuminate\Http\JsonResponse
     */
//    function index($id,$resturantId){
//        if( CartItem::where([['user_id',$id],['restaurant_id',$resturantId]])->first()){
//            $article =CartItem::with('item')->where([['user_id','=',$id],['restaurant_id',$resturantId]])->get();
//            if ($article) {
//                return response()->json(['success' => true, "message" => "success", "data" => $article]);
//            }else{
//                return response()->json(['success' => false, "message" => "There is no items yet" ]);
//            }
//        }else{
//            return response()->json(['success' => false, "message" => "there is nothing to checkout", "data" => []],200);
//        }
//
//    }

     function index($id,$resturantId)
     {
         try {
             if (CartItem::where([['user_id',$id],['restaurant_id',$resturantId]])->first()) {
                  $article =CartItem::with('item')->where([['user_id','=',$id],['restaurant_id',$resturantId]])->get();
                 return response()->json([

                      'success' => true,
                      "message" => "success",
                      'data' => $article,

                 ], 200);
             }
             return response()->json([
                      'success' => false,
                      "message" => "There is no items yet",
             ], 404);
         } catch (\Exception $e) {
             return response()->json([
                 'message' => $e
             ], 500);
         }
     }

    /**
     * Save cart item
     *
     * @param AddCartItemRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    function store(AddCartItemRequest $request){
        $item= new CartItem();
        $item->fill($request->all());
        $item->save();
        return response()->json(['success' => true, 'message' => "Successfully added", 'data' => $item]);

    }

    /**
     *
     * delete cart item depending on id
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    function destroy($id){
        try {
            $item= CartItem::where('id',$id)->first();
            if($item){
                $item->delete();
                return response()->json(['success'=>true,'message'=>'Successfully deleted','data'=>$id]);
            }
            else{
                return response()->json(['success'=>false,'message'=>'the item does not exist'], 404);
            }
        }catch (\Exception $e){
            return response()->json(['success'=>false,'message'=>'Something wrong please try again']);
        }

    }

    /**
     * Update cart item depending on id
     *
     * @param $id
     * @param UpdateCartItemQuantity $request
     * @return \Illuminate\Http\JsonResponse
     */
    function update($id, UpdateCartItemQuantity $request){
            $inputs = $request->all();
            $quantity =CartItem::where('id',$id)->update($inputs);
            $cartItem = CartItem::where('id',$id)->first();
            if ($quantity){
                return response()->json(
                    ['success' => true,
                        'message' => " Successfully updated" ,
                        "data"=>$cartItem]);
            }
            else{
                return response()->json(['success' => false, 'message' => " the user does not exist"]);
            }

    }

    /**
     *
     * create new order and transfer from cart item table into order item table
     *
     * @param CheckoutRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    function checkout(CheckoutRequest $request){
        try {
            $inputs = $request->all();


 //           $items = CartItem::where([['user_id', '=', $inputs['user_id']], ['restaurant_id', $inputs['restaurant_id']]])->get();


//            $items = CartItem::where([['user_id', '=', $inputs['user_id']], ['restaurant_id', $inputs['restaurant_id']]])->get();
            $items = $inputs['cart_items'];

            if (count($items) > 0) {

                $inputs['is_paid'] = 0;
                $order = new Order();
                $order->fill($inputs);
                $order->save();
                $orderId = $order->id;
                foreach ($items as $item) {
                    $orderItem = new OrderItem();
                    $orderItem->quantity = $item['quantity'];
                    $orderItem->item_id = $item['item_id'];
                    $orderItem->order_id = $orderId;
                    $orderItem->user_id = $item['user_id'];
                    $price = Item::where('id', $item['item_id'])->get('price');
                    $orderItem->price = $price[0]->price;
                    $orderItem->save();
                    CartItem::where([['user_id',$inputs['user_id']],['restaurant_id',$inputs['restaurant_id']]])->delete();
                }
                return response()->json(["success" => true, "message" => "Thank you for your order"]);
            } else {
                return response()->json(["success" => false, "message" => "there is nothing to checkout"]);
            }
        }catch (\Exception $e){
            return response()->json(['success'=>false,'message'=>'Something wrong please try again']);
        }

    }
}



