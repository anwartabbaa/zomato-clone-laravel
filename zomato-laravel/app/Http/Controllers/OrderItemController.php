<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Models\OrderItem;

use App\Http\Requests\AddOrderItemRequest;
use App\Http\Requests\UpdateOrderItemRequest;
class OrderItemController extends Controller
{
    public function addOrderItem(AddOrderItemRequest $request)
    {
        $inputs = $request->validated();
        $item = new OrderItem();
        $item->quantity = $request->input('quantity');
        $item->price = $request->input('price');
        $item->item_id = $request->input('item_id');
        $item->order_id = $request->input('order_id');
        $item->save();
        return response()->json([
            'success'=>true,
            'message'=>'OrderItem added successfully',
            'data'=>$item,
        ]);
    }


    function update($id, UpdateOrderItemRequest $request)
    {

        $inputs = $request->validated();
        $item = OrderItem::find($id);
        $item->update($inputs);
        $item = OrderItem::find($id);

        return response()->json([
            'success'=>true,
            'message'=>"order item updated successfully",
            'data'=>$item
        ]);
    }

    function delete($id)
    {
        $orderItem = OrderItem::where('id', $id)->first();
        $result = $orderItem->delete();
        if ($result) {
            return response()->json([
                'success'=>true,
                'message'=>'order item has been deleted successfully',
                'data'=>$orderItem,
            ]);
        } else {
            return response()->json([
                'success'=>false,
                'message'=>'rder item hasn\'t been deleted',
                'data'=>$orderItem,
            ],400);
        }
    }

    function index()
    {
        try {
            $items = OrderItem::all();
            if ($items->isNotEmpty()) {
                return response()->json([
                    'success'=>true,
                    'message'=>'OrderItem fetched successfully',
                    'data' => $items
                ], 200);
            }
            return response()->json([
                'success'=>false,
                'message'=>'No OrderItem exist',
                'data' => $items
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'success'=>false,
                'message' => $e
            ], 500);
        }
    }

}
