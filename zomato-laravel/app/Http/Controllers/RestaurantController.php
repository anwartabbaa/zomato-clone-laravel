<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddRestaurantRequest;
use App\Http\Requests\UpdateRestaurantRequest;
use App\Models\Driver;
use App\Models\Restaurant;
use App\Repository\RestaurantInfoRepositoryInterface;
use App\Repository\RestaurantRepositoryInterface;
use Illuminate\Http\Request;
use function PHPUnit\Framework\isEmpty;

class RestaurantController extends Controller
{
    /**
     * @var RestaurantRepositoryInterface
     */
    private RestaurantRepositoryInterface $restaurantRepository;
    private RestaurantInfoRepositoryInterface $restaurantInfoRepository;

    /**
     * @param RestaurantRepositoryInterface $restaurantRepository
     */
    public function __construct(RestaurantRepositoryInterface $restaurantRepository,RestaurantInfoRepositoryInterface $restaurantInfoRepository)
    {
        $this->restaurantRepository = $restaurantRepository;
        $this->restaurantInfoRepository = $restaurantInfoRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $restaurant = $this->restaurantRepository->all();

        if(!$restaurant->isEmpty()){
            return response()->json([
             'success'=>true,
                'message'=>'Restaurants retrieved successfully',
                'data'=>$restaurant,
            ]);
        }else{
            return response()->json([
                'success'=>false,
                'message'=>'Restaurants retrieved successfully',
                'data'=>$restaurant,
            ],404);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(AddRestaurantRequest $request)
    {
        $inputs = $request->validated();
        $restaurant = $this->restaurantRepository->create($inputs);

        return response()->json([
                'success'=>true,
                'message'=>'Restaurants saved successfully',
                'data'=>$restaurant,
        ]);

    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(int $id)
    {
        $restaurant = $this->restaurantRepository->find($id);
        if($restaurant){
            return response()->json([
                'success'=>true,
                'message'=>'Restaurants retrieved successfully',
                'data'=>$restaurant,
            ]);
        }else{
            return response()->json([
                'success'=>false,
                'message'=>'Restaurants retrieved successfully',
                'data'=>$restaurant,
            ],404);
        }
    }


    /**
     * @param UpdateRestaurantRequest $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateRestaurantRequest $request, int $id)
    {

        $inputs = $request->all();
        $inputs['restaurant_id']=$id;
        $updatedRestaurant =  $this->restaurantRepository->update($inputs,$id);
        if($resInfo = $this->restaurantInfoRepository->findByrestaurantid($id)){
//            $reqInfo = $this->restaurantInfoRepository->update($inputs,$resInfo->id);
            if(isset($inputs['logo']) && $inputs['logo'] !== ""){
                if($resInfo->logo === null){
                    $inputs['logo'] = addPhotoFile($request);
                }else{
                    if(deletePhotoFile($resInfo->logo)){
                        $inputs['logo'] = addPhotoFile($request);
                    }else{
                        return  response()->json([
                            'success'=>false,
                            'message'=>'restaurant logo photo wasn\'t  updated' ,
                            'data'=> ''
                        ],400);
                    }
                }
            }

            $restaurantInfoUpdated = $this->restaurantInfoRepository->update($inputs,$resInfo->id);

        }else{
            $inputs['logo'] = addPhotoFile($request);
            $reqInfo = $this->restaurantInfoRepository->create($inputs);
        }
        $updatedRestaurant =  $this->restaurantRepository->update($inputs,$id);
        $restaurantInfo = $this->restaurantInfoRepository->findByrestaurantid($id);



        $updatedRestaurant =  $this->restaurantRepository->find($id);

        if($updatedRestaurant || $restaurantInfoUpdated){

            return response()->json([
                'success'=>true,
                'message'=>'Restaurants updated successfully',
                'data'=>$updatedRestaurant,
            ]);
        }else{
            return response()->json([
                'success'=>false,
                'message'=>'Restaurants wasn\'t updated successfully',
                'data'=>$updatedRestaurant,
            ],400);
        }

    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(int $id)
    {
        $restaurant = $this->restaurantRepository->find($id);
       $deletedRestaurant = $this->restaurantRepository->delete($id);

        if($deletedRestaurant){

            if($restaurant->restaurant_info){
                $this->restaurantInfoRepository->delete($restaurant->restaurant_info->id);
            }

            return response()->json([
                'success'=>true,
                'message'=>'Restaurants deleted successfully',
                'data'=>$deletedRestaurant,
            ]);
        }else{
            return response()->json([
                'success'=>false,
                'message'=>'Restaurant not deleted',
                'data'=>$deletedRestaurant,
            ],400);
        }
    }
    public function getRestaurant($id){
        return Restaurant::find($id);
    }
}
