<?php

namespace App\Http\Controllers;


use App\Models\Order;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    public function addOrder(Request $request)
    {
        $inputs = $request->all();
        $order = new Order();
        $order->fill($inputs);
        $order->save();



        return response()->json([
            'success'=>true,
            'message'=>'Order saved successfully',
            'data'=>$order,
        ]);

    }

    function delete($id)
    {
        $order = Order::where('id', $id)->first();
        $result = $order->delete();
        if ($result) {
            return response()->json([
                'success'=>true,
                'message'=>"Order item deleted successfully",
                'data'=>$order
            ]);
        } else {
            return response()->json([
                'success'=>false,
                'message'=>"Order hasn't been deleted successfully",
                'data'=>$order
            ],400);
        }
    }

    function updateOrder($id, Request $req)
    {
        $inputs = $req->all();
        $item = Order::find($id);
        $item->update($inputs);
        $item = Order::find($id);

        return response()->json([
            'success'=>true,
            'message'=>"Order item updated successfully",
            'data'=>$item
        ]);
    }

    function index(Request $request)
    {
        try {
            if(isset($request->restaurant) && $request->restaurant != "" ){
                $items = Order::where("restaurant_id",$request->restaurant)->get();
            }else{
                $items = Order::all();
            }
            if ($items) {
                return response()->json([
                    'success'=>true,
                    'message'=>"Order fetched successfully",
                    'data' => $items
                ], 200);
            }
            return response()->json([
                'success'=>false,
                'message'=>"No Order exist",
                'item' => $items
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e
            ], 500);
        }
    }

    public function getOrder($id){
        return Order::find($id);
    }
}
