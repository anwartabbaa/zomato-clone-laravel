<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddRestaurantInfoRequest;
use App\Models\RestaurantInfo;
use App\Repository\RestaurantInfoRepositoryInterface;
use Illuminate\Http\Request;

class RestaurantInfoController extends Controller
{
    /**
     * @var RestaurantInfoRepositoryInterface
     */
    private $restaurantInfoRepository;

    /**
     * @param RestaurantInfoRepositoryInterface $restaurantInfoRepository
     */
    public function __construct(RestaurantInfoRepositoryInterface $restaurantInfoRepository)
    {
        return $this->restaurantInfoRepository = $restaurantInfoRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $restaurantInfoRepository = $this->restaurantInfoRepository->all();

        if(!$restaurantInfoRepository->isEmpty()){
            return response()->json([
               'success'=>true,
               'message'=>'restaurant information fetched successfully',
               'data'=> $restaurantInfoRepository
            ]);
        }else{
            return response()->json([
                'success'=>false,
                'message'=>'No restaurant information available',
                'data'=> $restaurantInfoRepository
            ],404);
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(AddRestaurantInfoRequest $request)
    {
        $inputs = $request->all();

        if($request->logo){
            $inputs['logo'] = addPhotoFile($request);
        }
        $restaurantInfo = $this->restaurantInfoRepository->create($inputs);

        if($restaurantInfo){
           return  response()->json([
                'success'=>true,
                'message'=>'restaurant information created successfully',
                'data'=> $restaurantInfo
            ]);
        }else{
            return response()->json([
                'success'=>false,
                'message'=>'restaurant information wasn\'t saved',
                'data'=> $restaurantInfo
            ],400);
        }
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(int $id)
    {
        $restaurantInfo = $this->restaurantInfoRepository->find($id);

        if($restaurantInfo){
            return  response()->json([
                'success'=>true,
                'message'=>'restaurant information fetched successfully',
                'data'=> $restaurantInfo
            ]);
        }else{
            return response()->json([
                'success'=>false,
                'message'=>'restaurant information doesn\'t exist',
                'data'=> $restaurantInfo
            ],404);
        }
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request,int $id)
    {
        $inputs = $request->all();
        $restaurantInfo = $this->restaurantInfoRepository->find($id);

//        if(isset($inputs['logo']) && $inputs['logo'] !== "" ){
//            if($restaurantInfo->logo === null){
//                $inputs['logo'] = addPhotoFile($request);
//            }else{
//                if(deletePhotoFile($restaurantInfo->logo)){
//                    $inputs['logo'] = addPhotoFile($request);
//                }else{
//                    return  response()->json([
//                        'success'=>false,
//                        'message'=>'restaurant logo photo wasn\'t  updated' ,
//                        'data'=> ''
//                    ],400);
//                }
//            }
//        }


        $restaurantInfoUpdated = $this->restaurantInfoRepository->update($inputs,$id);

        if($restaurantInfoUpdated){
            return  response()->json([
                'success'=>true,
                'message'=>'restaurant information updated successfully',
                'data'=> $restaurantInfoUpdated
            ]);
        }else{
            return response()->json([
                'success'=>false,
                'message'=>'restaurant information wasn\'t updated',
                'data'=> $restaurantInfoUpdated
            ],400);
        }

    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(int $id)
    {
        $restaurantInfo = $this->restaurantInfoRepository->find($id);

        if($restaurantInfo->logo){
            if(!deletePhotoFile($restaurantInfo->logo)) {
                return response()->json([
                    'success'=>false,
                    'message'=>'restaurant photo wasn\'t deleted',
                ],400);
            }
        }
        $restaurantInfoDeleted = $this->restaurantInfoRepository->delete($restaurantInfo->id);

        if($restaurantInfoDeleted){
            return  response()->json([
                'success'=>true,
                'message'=>'restaurant information deleted successfully',
                'data'=> $restaurantInfoDeleted
            ]);
        }else{
            return response()->json([
                'success'=>false,
                'message'=>'restaurant information wasn\'t deleted',
                'data'=> $restaurantInfoDeleted
            ],400);
        }
    }
}
