<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddCategoryRequest;
use App\Models\Category;
use App\Models\Driver;
use App\Repository\CategoryRepositoryInterface;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * @var CategoryRepositoryInterface
     */
    private CategoryRepositoryInterface $categoryRepository;

    /**
     * @param CategoryRepositoryInterface $categoryRepository
     */
    public function __construct(CategoryRepositoryInterface $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $categories = $this->categoryRepository->all();

        if(!$categories->isEmpty()){
            return response()->json([
               'success'=>true,
               'message'=>'All categories fetched successfully',
               'data'=>$categories
            ]);
        }else{
            return response()->json([
                'success'=>false,
                'message'=>'No categories exists',
                'data'=>$categories
            ]);
        }
    }

    /**
     * @param AddCategoryRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(AddCategoryRequest $request)
    {
        $category = $this->categoryRepository->create($request->all());

        return response()->json([
            'success'=>true,
            'message'=>'Category has been created successfully',
            'data'=>$category
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(int $id)
    {
        $category = $this->categoryRepository->find($id);

        if($category){

            return response()->json([
                'success'=>true,
                'message'=>'Category fetched successfully',
                'data'=>$category
            ]);
        }else{

            return response()->json([
                'success'=>false,
                'message'=>'No category exist',
                'data'=>$category
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, int $id)
    {
        $updatedCategory = $this->categoryRepository->update($request->all(),$id);

        if($updatedCategory){

                return response()->json([
                    'success'=>true,
                    'message'=>'Category updated successfully',
                    'data'=>$updatedCategory
                ]);
        }else{

                return response()->json([
                    'success'=>false,
                    'message'=>'Category wasn\'t updated' ,
                    'data'=>$updatedCategory
                ]);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(int $id)
    {
        $deletedCategory = $this->categoryRepository->delete($id);

        if($deletedCategory){
            return response()->json([
                'success'=>true,
                'message'=>'Category deleted successfully',
                'data'=>$deletedCategory
            ]);
        }else{

            return response()->json([
                'success'=>false,
                'message'=>'Category wasn\'t deleted' ,
                'data'=>$deletedCategory
            ]);
        }
    }
    public function getCategory($id){
        return Category::find($id);
    }
    public function getCategoriesByResturant($id){
        return Category::where('restaurant_id',$id)->get();
    }
}
