<?php

namespace App\Http\Controllers;


use App\Http\Requests\AddUserInfo;
use App\Models\Driver;

use App\Models\User;
use App\Models\UserInfo;
use Illuminate\Http\Request;

class UserInfoController extends Controller
{
    public function addUserInfo(AddUserInfo $request)
    {
        $inputs = $request->validated();

        $userinfo = new UserInfo();
        $userinfo->fill($inputs);
//        $userinfo->photo=$request->file('photo')->store('products');
        if($userinfo->save()){
            $user = User::where('id',$inputs['user_id'])->with('user_info')->first();
        }


        return response()->json([
            'success'=>true,
            'message'=>'User saved successfully',
            'data'=>$user,
        ]);

    }

    function delete($id)
    {
        $userinfo = UserInfo::where('id', $id)->first();
        $result = $userinfo->delete();
        if ($result) {

            return response()->json([
                'success'=>true,
                'message'=>'Userinfo has been deleted successfully',
                'data'=>$userinfo,
            ]);
        } else {
            return response()->json([
                'success'=>false,
                'message'=>'Userinfo hasn\'t been deleted',
                'data'=>$result,
            ],400);
        }
    }
    function updateUserInfo($id, Request $req)
    {

        $inputs = $req->all();

        $userinfo = UserInfo::find($id);
        if ($req->file('photo')) {
            $userinfo->photo = $req->file('photo')->store('products');
        }
        $userinfo->update($inputs);
        $userinfo = UserInfo::find($id);

        $user = User::where('id',$userinfo->user_id)->with('user_info')->first();

        $user->update($inputs);

        return response()->json([
            'success'=>true,
            'message'=>"user updated successfully",
            'data'=>$user
        ]);
    }

    function index()
    {
        try {
            $items = UserInfo::all();
            if ($items) {
                return response()->json([
                    'data' => $items
                ], 200);
            }
            return response()->json([
                'item' => "empty"
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e
            ], 500);
        }
    }

    public function getUserInfo($id){
        return UserInfo::find($id);
    }
}

