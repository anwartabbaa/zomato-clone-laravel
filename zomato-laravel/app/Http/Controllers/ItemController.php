<?php

namespace App\Http\Controllers;

use App\Models\Driver;
use Illuminate\Http\Request;
use App\Models\Item;
use App\Http\Requests\AddItemRequest;
use App\Http\Requests\UpdateItemRequest;

class ItemController extends Controller
{
    public function addItem(AddItemRequest $request)
    {
        $inputs = $request->validated();
        $item = new Item();
        $item->name = $request->input('name');
        $item->description = $request->input('description');
        $item->price = $request->input('price');
        $item->categorie_id = $request->input('categorie_id');
        $item->restaurant_id = $request->input('restaurant_id');
        $imageName = time().'.'.$request->photo->extension();
        $request->photo->move(public_path('images'), $imageName);
        $item->photo= "images/".$imageName;
        $item->save();
        return response()->json([
            'success'=>true,
            'message'=>'Item added successfully',
            'data'=>$item,
        ]);
    }


    function update($id, UpdateItemRequest $request)
    {

        $inputs = $request->all();
        $item = Item::find($id);

        if(isset($inputs['photo']) && $inputs['photo'] !== "" ){
            if($item->photo === null){
                $inputs['photo'] = addPhotoFile($request);
            }else{
                if(deletePhotoFile($item->photo)){
                    $inputs['photo'] = addPhotoFile($request);
                }else{
                    return  response()->json([
                        'success'=>false,
                        'message'=>'Item photo wasn\'t  updated' ,
                        'data'=> ''
                    ],400);
                }
            }
        }


        $item->update($inputs);
        $item = Item::find($id);

        return response()->json([
            'success'=>true,
            'message'=>"Item updated successfully",
            'data'=>$item
        ]);
    }

    function delete($id)
    {
        $item = Item::where('id', $id)->first();
        if(deletePhotoFile($item->photo)){
            $result = $item->delete();
        }else{
            return response()->json([
                'success'=>false,
                'message'=>"Photo hasn't been deleted successfully",
                'data'=>$item
            ],400);
        }

        if ($result) {
            return response()->json([
                'success'=>true,
                'message'=>"Item deleted successfully",
                'data'=>$item
            ]);
        } else {
            return response()->json([
                'success'=>false,
                'message'=>"Item hasn't been deleted successfully",
                'data'=>$item
            ],400);
        }
    }
    function index()
    {
        try {
            $items = Item::all();
            if ($items) {
                return response()->json([
                    'success'=>true,
                    'message'=>"Item has been fetched successfully",
                    'data' => $items
                ], 200);
            }
            return response()->json([
                'success'=>false,
                'message'=>"No item hasn't been fetched",
                'item' => $items
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e
            ], 500);
        }
    }
    public function getItem($id){
        return Item::find($id);
    }
}
