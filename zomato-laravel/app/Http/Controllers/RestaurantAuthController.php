<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddRestaurantRequest;
use App\Models\Restaurant;
use App\Repository\RestaurantRepositoryInterface;
use Illuminate\Http\Request;

class RestaurantAuthController extends Controller
{
    /**
     * @var RestaurantRepositoryInterface
     */
    private RestaurantRepositoryInterface $restaurantRepository;

    /**
     * @param RestaurantRepositoryInterface $restaurantRepository
     */
    public function __construct(RestaurantRepositoryInterface $restaurantRepository)
    {
        $this->restaurantRepository = $restaurantRepository;
    }

    /**
     * @param AddRestaurantRequest $request
     * @return mixed
     */
    public function register(AddRestaurantRequest $request)
    {
       $restaurant = $this->restaurantRepository->create($request->validated());

        $token = auth('restaurant')->login($restaurant);

        return $this->respondWithToken($token,$restaurant);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['email', 'password']);

        if (!$token = auth('restaurant')->attempt($credentials)) {
            return response()->json([
                'success'=>false,
                'message' => 'Wrong credentials provided'
            ], 401);
        }
        $restaurant= auth('restaurant')->user();
        $restaurant = $this->restaurantRepository->find($restaurant->id);
        return $this->respondWithToken($token,$restaurant);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth('restaurant')->logout();

        return response()->json([
            'success'=>true,
            'message' => 'Successfully logged out'
        ]);
    }

    /**
     * @param $token
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token,$restaurant)
    {
        return response()->json([
            'success'=>true,
            'data'=>$restaurant,
            'access_token' => $token,
            'token_type'   => 'bearer',
            'expires_in'   => auth('restaurant')->factory()->getTTL() * 60
        ]);
    }
}
