<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateUserRequest;
use App\Http\Requests\UserLoginRequest;
use App\Http\Requests\UserRegisterRequest;
use App\Models\Driver;
use App\Models\User;
use App\Models\UserInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PHPUnit\Exception;

class UserController extends Controller
{
    public function register(UserRegisterRequest $request)
    {
        $user = User::create([
            'name' => $request->name,
            'email'    => $request->email,
            'password' => $request->password,
        ]);

        $token = auth('users')->login($user);

        return $this->respondWithToken($token,$user);
    }

    public function login(UserLoginRequest $request)
    {
        $credentials = request(['email', 'password']);

        if (! $token = auth('users')->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        $user = auth('users')->user();
        $user= User::where('id',$user->id)->with('user_info')->first();

        return $this->respondWithToken($token,$user);
    }

    public function logout()
    {
        auth('users')->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    protected function respondWithToken($token,$user)
    {
        return response()->json([
            'success'=>true,
            'message'=>"Successfully logged in",
            "data"=>$user,
            'access_token' => $token,
            'token_type'   => 'bearer',
            'expires_in'   => auth('users')->factory()->getTTL() * 60
        ]);
    }
    function index(){
        try {
            $users= User::with('user_info','order_item')->get();
            if($users->first()!="") {
                return response()->json(['success' => true, 'message' =>"success","data"=> $users], 200);
            }
            else{
                return response()->json(['success' => true, 'message' =>'there are no users yet'], 200);
            }
        }
         catch (Exception $e){
           return response()->json(['success'=>false,'message'=>'Something wrong please try again']);
         }
    }
    function destroy($id){
        try {
            $user = User::where('id',$id)->first();
        $item = $user->delete();
      if ($item) {
          $userinfo = UserInfo::where('user_id', $id)->first();
          $userinfo->delete();
          return response()->json(['success' => true, 'message' => "Successfully deleted", 'data' => $id]);
      }
      else{
          return response()->json(['success' => false, 'message' => " the user does not exist"]);
      }
        }catch (\Exception $e){
            return response()->json(['success'=>false,'message'=>'Something wrong please try again']);
        }

    }
    function update(UpdateUserRequest $request , $id ){
//        try {
            $inputs = $request->all();

//            $user =User::where('id',$id)->update(['name' => $request->name, 'password' => bcrypt($request->password)]);
            $user =User::where('id',$id)->update($inputs);
            $updatedUser = User::with('user_info')->find($id);
            if ($user){
                return response()->json(['success' => true, 'message' => " Successfully updated" ,"data"=>$updatedUser]);
            }
            else{
                return response()->json(['success' => false, 'message' => " the user does not exist"]);
            }

//        }catch (\Exception $e){
//            return response()->json(['success'=>false,'message'=>'Something wrong please try again']);
//        }


    }

    public function getUser($id){
        return User::with('user_info','order_item.itemzz')->find($id);
    }
}
