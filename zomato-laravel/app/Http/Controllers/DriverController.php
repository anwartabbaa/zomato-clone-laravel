<?php

namespace App\Http\Controllers;

use App\Http\Requests\AdddriverRequest;

use App\Http\Requests\RegisterdriverRequest;
use App\Models\Driver;

use Illuminate\Http\Request;

class DriverController extends Controller
{
    public function register(RegisterdriverRequest $request)
    {
        $inputs = $request->validated();
        $driver = new Driver();
        $driver->fill($inputs);
        $driver->save();

        $token = auth('driver')->login($driver);

        return $this->respondWithToken($token);
    }

    public function login()
    {
        $credentials = request(['email', 'password']);

        if (! $token = auth('driver')->attempt($credentials)) {
           return response()->json([
                'success'=>false,
                'message' => 'Wrong credentials provided'
            ], 401);
        }
        $driver = auth('driver')->user();
        $driver= Driver::where('id',$driver->id)->with('restaurant')->first();
        return $this->respondWithToken($token,$driver);
    }

    public function logout()
    {
        auth('driver')->logout();

        return response()->json([
            'success'=>true,
            'message' => 'Successfully logged out'
        ]);
    }

    protected function respondWithToken($token,$driver)
    {
        return response()->json([
            'success'=> true,
            'data' => $driver,
            'access_token' => $token,
            'token_type'   => 'bearer',
            'expires_in'   => auth('driver')->factory()->getTTL() * 60
        ]);
    }
    public function adddriver(AdddriverRequest $request)
    {
        $inputs = $request->validated();
        $driver = new Driver();
        $driver->fill($inputs);
        $driver->save();


        return response()->json([
            'success'=>true,
            'message'=>'Driver saved successfully',
            'data'=>$driver,
        ]);

    }

    function delete($id)
    {
        $driver = Driver::where('id', $id)->first();
        $result = $driver->delete();
        if ($result) {

            return response()->json([
                'success'=>true,
                'message'=>'driver has been deleted successfully',
                'data'=>$driver,
            ]);
        } else {
            return response()->json([
                'success'=>false,
                'message'=>'driver hasn\'t been deleted',
                'data'=>$result,
            ],400);
        }
    }
    function updateDriver($id, Request $req)
    {
        $inputs = $req->all();
        $driver = Driver::find($id);
        $driver->update($inputs);
        $driver = Driver::find($id);

        return response()->json([
            'success'=>true,
            'message'=>"admin updated successfully",
            'data'=>$driver
        ]);
    }

    function index(Request $request)
    {
        try {

            if(isset($request->restaurant) && $request->restaurant != "" ){
                $items = Driver::where("restaurant_id",$request->restaurant)
                    ->with('driver_order')->get();
            }else{
                $items = Driver::all();
            }

            if ($items) {
                return response()->json([
                    'data' => $items
                ], 200);
            }
            return response()->json([
                'item' => "empty"
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e
            ], 500);
        }
    }

    public function getDriver($id){
        return Driver::with('driver_order')->find($id);
    }
}


