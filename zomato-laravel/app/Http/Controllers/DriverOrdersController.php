<?php

namespace App\Http\Controllers;

use App\Http\Requests\AdddriverRequest;
use App\Models\Admin;
use App\Models\Driver;
use App\Models\DriverOrder;
use Illuminate\Http\Request;

class DriverOrdersController extends Controller
{
    public function adddriver_order(Request $request)
    {
        $inputs = $request->all();
        $driver_order = new DriverOrder();
        $driver_order->fill($inputs);
        $driver_order->save();

        $driver = Driver::find($request->driver_id);



            return response()->json([
                'success'=>false,
                'message'=>'Driver not available'
            ],400);




        return response()->json([
            'success'=>true,
            'message'=>'Driver saved successfully',
            'data'=>$driver_order,
        ]);

    }
    function delete($id)
    {
        $driver_order = DriverOrder::where('id', $id)->first();
        $result = $driver_order->delete();
        if ($result) {

            return response()->json([
                'success'=>true,
                'message'=>'driver order has been deleted successfully',
                'data'=>$driver_order,
            ]);
        } else {
            return response()->json([
                'success'=>false,
                'message'=>'driver order hasn\'t been deleted',
                'data'=>$result,
            ],400);
        }
    }

    function updateDriverOrder($id, Request $req)
    {
        $inputs = $req->all();
        $inputs['is_delivered'] = 1;

        $item = DriverOrder::find($id);
        $item->update($inputs);
        $item = DriverOrder::find($id);

        $driver = Driver::find($item->driver->id);

        if(!$driver->is_available){
            $driver->is_available= 1;
            $driver->update([$driver]);
        }

        return response()->json([
            'success'=>true,
            'message'=>"driver order item updated successfully",
            'data'=>$item
        ]);
    }

    function index($id)
    {
        try {

            $items = DriverOrder::where('driver_id',$id)->with('order.user','order.item')->first();
            if ($items) {
                return response()->json([
                    'data' => $items
                ], 200);
            }
            return response()->json([
                'item' => "empty"
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e
            ], 500);
        }
    }
}
