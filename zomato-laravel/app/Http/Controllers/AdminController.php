<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddAdminRequest;
use App\Http\Requests\UpdateAdminRequest;
use App\Models\Admin;

use Illuminate\Http\Request;


class AdminController extends Controller
{


    public function login()
    {
        $credentials = request(['email', 'password']);

        if (! $token = auth('admin')->attempt($credentials)) {
            return response()->json([
                'success'=>false,
                'message' => 'Wrong credentials provided'
            ], 401);
        }

        return $this->respondWithToken($token);
    }

    public function logout()
    {
        auth('admin')->logout();

        return response()->json([
            'success'=>true,
            'message' => 'Successfully logged out'
        ]);
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'success'=>true,
            'access_token' => $token,
            'token_type'   => 'bearer',
            'expires_in'   => auth('admin')->factory()->getTTL() * 60
        ]);
    }

    function delete($id)
    {
        $admin = Admin::where('id', $id)->first();
        $result = $admin->delete();
        if ($result) {

            return response()->json([
                'success'=>true,
                'message'=>'admin has been deleted successfully',
                'data'=>$admin,
            ]);
        } else {
            return response()->json([
                'success'=>false,
                'message'=>'admin hasn\'t been deleted',
                'data'=>$result,
            ],400);
        }
    }

    public function addadmin(AddAdminRequest $request)
    {
        $inputs = $request->validated();
        $admin = new Admin();
        $admin->fill($inputs);
        $admin->save();
        return response()->json([
            'success'=>true,
            'message'=>'admin added successfully',
            'data'=>$admin,
        ]);
    }


    function update($id, UpdateAdminRequest $req)
    {

        $inputs = $req->validated();
        $admin = Admin::find($id);
        $admin->update($inputs);
        $admin = Admin::find($id);

        return response()->json([
            'success'=>true,
            'message'=>"admin updated successfully",
            'data'=>$admin
        ]);
    }

    function index()
    {
        try {
            $items = Admin::all();
            if ($items->isNotEmpty()) {
                return response()->json([
                    'data' => $items
                ], 200);
            }
            return response()->json([
                'item' => "empty"
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e
            ], 500);
        }
    }

    public function getAdmin($id){
        return Admin::find($id);
    }
}
