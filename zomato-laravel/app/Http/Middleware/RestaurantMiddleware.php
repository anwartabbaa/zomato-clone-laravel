<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class RestaurantMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(auth('restaurant')->check()){
            return $next($request);
        }
        return response()->json([
            'success'=>false,
            'message' => 'you must be logged in'
        ]);
    }
}
