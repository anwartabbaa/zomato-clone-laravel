<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;

class UpdateRestaurantRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>[''],
            'email'=>['unique:restaurants,email,'.$this->detail],
            'password'=>[''],
            'status'=>['']
        ];
    }

    public function messages()
    {
        return [
            'unique'=> ':attribute must be unique',
        ];
    }

    public function attributes()
    {
        return [
            'name'=>'Name',
            'email'=>'Email',
            'password'=>'Password'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = collect($validator->errors());
        $errors = $errors->collapse();


        $response = response()->json([
            'success' => false,
            'message' => 'Ops! Some errors occurred',
            'errors' => $errors
        ],400);

        throw (new ValidationException($validator, $response));
    }
}
