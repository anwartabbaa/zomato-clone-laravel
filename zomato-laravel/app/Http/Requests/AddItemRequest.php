<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;

class AddItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>['required'],
            'description'=>['required'],
            'price'=>['required'],
            'categorie_id'=>['required'],
            'restaurant_id'=>['required'],
            'photo'=>'required|image|mimes:jpeg,png,jpg,gif,svg',
        ];
    }
    public function messages()
    {
        return [
            'required'=> ':attribute must be provided',
        ];
    }

    public function attributes()
    {
        return [
            'name'=>'name',
            'description'=>'description',
            'price'=>'price',
            'categorie_id'=>'categorie_id',
            'restaurant_id'=>'restaurant_id'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = collect($validator->errors());
        $errors = $errors->collapse();


        $response = response()->json([
            'success' => false,
            'message' => 'Ops! Some errors occurred',
            'errors' => $errors
        ]);

        throw (new ValidationException($validator, $response));
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

}
