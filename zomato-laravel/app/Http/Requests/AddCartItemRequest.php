<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;

class AddCartItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id'=>'required',
            'item_id'=>'required',
            'quantity'=>'required',
            'restaurant_id'=>'required'
        ];
    }

    public function messages()
    {
        return [
            'required'=>':attribute must be provided'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
      $errors = collect($validator->errors());
      $errors = $errors->collapse();

      $response = response()->json([
          'success'=>false,
          'message'=>'Some errors occured',
          'errors'=>$errors
      ]);
      throw (new ValidationException($validator,$response));
    }
}
