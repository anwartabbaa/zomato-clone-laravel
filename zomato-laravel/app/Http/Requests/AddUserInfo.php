<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;

class AddUserInfo extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone'=>['required','unique:user_infos'],
            'address1'=>['required'],
            'address2'=>['required'],
            'user_id'=>['required','unique:user_infos'],
        ];
    }

    public function messages()
    {
        return [
            'required'=> ':attribute must be provided',
            'unique'=> ':attribute must be unique',
        ];
    }

    public function attributes()
    {
        return [
            'name'=>'Name',
            'email'=>'Email',
            'phone'=>'phone',
            'address1'=>'address1',
            'address2'=>'address2',
            'user_id'=>'user_id',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = collect($validator->errors());
        $errors = $errors->collapse();


        $response = response()->json([
            'success' => false,
            'message' => 'Ops! Some errors occurred',
            'errors' => $errors
        ]);

        throw (new ValidationException($validator, $response));
    }
}
