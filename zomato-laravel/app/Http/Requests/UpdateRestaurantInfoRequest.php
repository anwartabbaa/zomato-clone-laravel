<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;

class UpdateRestaurantInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'logo'=>[''],
            'mobile'=>[''],
            'tel'=>[''],
            'address'=>[''],
            'restaurant_id'=>['']
        ];
    }

    /**
     * @return string[]
     */
    public function messages()
    {
        return [
            ''=>':attribute must be provided'
        ];
    }

    /**
     * @return string[]
     */
    public function attributes()
    {
        return [
            'logo'=>'Logo',
            'mobile'=>'Mobile',
            'tel'=>'Telephone',
            'address'=>'Address'
        ];
    }

    /**
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = collect($validator->errors());
        $errors = $errors->collapse();


        $response = response()->json([
            'success' => false,
            'message' => 'Ops! Some errors occurred',
            'errors' => $errors
        ]);

        throw new (new ValidationException($validator,$response));
    }
}
