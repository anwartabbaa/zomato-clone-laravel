<?php

namespace App\Http\Requests;


use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;

class UpdateAdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'name'=>[''],
            'email'=>['unique:admins,email,'.$this->admin],
            'password'=>['']
        ];
    }

    public function messages()
    {
        return [
            ''=>':attribute must be provided'
        ];
    }

    public function attributes()
    {
        return [
            'name'=>'name',
            'email'=>'email',
            'password'=>'password'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = collect($validator->errors());
        $errors = $errors->collapse();


        $response = response()->json([
            'success' => false,
            'message' => 'Ops! Some errors occurred',
            'errors' => $errors
        ]);

        throw (new ValidationException($validator, $response));
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

}
