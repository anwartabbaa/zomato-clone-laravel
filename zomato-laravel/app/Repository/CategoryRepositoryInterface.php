<?php

namespace App\Repository;

use Illuminate\Support\Collection;

interface CategoryRepositoryInterface{
    /**
     * @return Collection
     */
    public function all():Collection;
}
