<?php
namespace App\Repository;

use App\Models\RestaurantInfo;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

interface RestaurantInfoRepositoryInterface
{
    public function all(): Collection;
    public function create(array $attributes): Model;
    public function update(array $attributes,$id): Model;
    public function findByrestaurantid($id): ?Model;
    public function delete($id): ?int;
}
