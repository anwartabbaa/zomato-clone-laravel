<?php

namespace App\Repository\Eloquent;

use App\Models\Driver;
use App\Repository\DriverRepositoryInterface;
use Illuminate\Support\Collection;

class DriverRepository extends BaseRepository implements DriverRepositoryInterface{

    /**
     * @param Driver $model
     */
      public function __construct(Driver $model)
      {
          parent::__construct($model);
      }

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->model->all();
    }
}
