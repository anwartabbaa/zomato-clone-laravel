<?php

namespace  App\Repository\Eloquent;

use App\Models\Restaurant;
use App\Repository\RestaurantRepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class RestaurantRepository extends BaseRepository implements  RestaurantRepositoryInterface {


    /**
     * @param Restaurant $model
     */
    public function __construct(Restaurant $model)
    {
        parent::__construct($model);
    }

    /**
     * @return Collection
     */
    public function all():Collection
    {
        return $this->model->with('restaurant_info','restaurant_category.items','drivers')->get();
    }

    /**
     * @param $id
     * @return Model|null
     */
    public function find($id): ?Model
    {
        return $this->model->where('id',$id)->with('restaurant_info','restaurant_category.items','drivers')->first();
    }
}
