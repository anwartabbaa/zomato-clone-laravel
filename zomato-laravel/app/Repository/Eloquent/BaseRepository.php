<?php

namespace App\Repository\Eloquent;

use App\Repository\EloquentRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class BaseRepository implements EloquentRepositoryInterface
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * BaseRepository constructor.
     *
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * @param array $attributes
     *
     * @return Model
     */
    public function create(array $attributes): Model
    {
        return $this->model->create($attributes);
    }

    /**
     * @param $id
     * @return Model
     */
    public function find($id): ?Model
    {
        return $this->model->find($id);
    }

    /**
     * @param array $attributes
     * @param $id
     * @return Model
     */
    public function update(array $attributes,$id): Model
    {
//   dd($attributes, $id);
        $updateModel = $this->find($id);
        $updateModel->update($attributes);
        $latestModel = $this->find($id);

        return $latestModel;
    }

    /**
     * @param $id
     * @return int|null
     */
    public function delete($id): ?int
    {
        $deleteModel = $this->find($id);
         $isDeleted = $deleteModel->delete();
//        dd($isDeleted);
         if($isDeleted){
             return $id;
         }

         return null;
    }

}
