<?php

namespace App\Repository\Eloquent;

use App\Models\RestaurantInfo;
use App\Repository\RestaurantInfoRepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class RestaurantInfoRepository extends BaseRepository implements RestaurantInfoRepositoryInterface
{

    /**
     * RestaurantInfoRepository constructor.
     *
     * @param RestaurantInfo $model
     */
    public function __construct(RestaurantInfo $model)
    {
        parent::__construct($model);
    }

    /**
     * @param $id
     * @return Model|null
     */

    public function findByrestaurantid($id): ?Model

    {
        return $this->model->where('restaurant_id',$id)->first();
    }

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->model->all();
    }
}
