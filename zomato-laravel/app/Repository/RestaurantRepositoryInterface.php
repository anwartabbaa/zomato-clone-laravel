<?php

namespace App\Repository;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

interface RestaurantRepositoryInterface{
    /**
     * @return Collection
     */
    public function all():Collection;

    /**
     * @param $id
     * @return Model|null
     */
    public function find($id): ?Model;
}
