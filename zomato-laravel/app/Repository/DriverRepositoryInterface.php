<?php

namespace App\Repository;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

interface  DriverRepositoryInterface {
    public function all(): Collection;
}
