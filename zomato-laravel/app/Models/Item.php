<?php

namespace App\Models;

use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;

    protected $fillable=[
        'price',
        'name',
        'description',
        'categorie_id',
        'restaurant_id',
        'photo'
    ];

    public function categories()
    {
        return $this->belongsTo(Category::class,'categorie_id','id');
    }
}
