<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DriverOrder extends Model
{
    use HasFactory;
    protected $fillable = [
        'order_id', 'driver_id','is_delivered'
    ];
    protected $table = "driver_orders";


    public function driver()
    {
        return $this->belongsTo(Driver::class);
    }
    public function order()
    {
        return $this->hasMany(Order::class, 'id' , 'order_id');
    }
}

