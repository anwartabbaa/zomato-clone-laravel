<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Restaurant;

class RestaurantInfo extends Model
{
    use HasFactory;

    protected $fillable = [
        'logo',
        'mobile',
        'tel',
        'address',
        'description',
        'restaurant_id'
    ];

    public function restaurant_info(){
        return $this->belongsTo(Restaurant::class);
    }
}
