<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\RestaurantInfo;
use App\Models\Category;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Restaurant extends Authenticatable implements JWTSubject
{
    use HasFactory;

    protected $fillable=[
        'name',
        'email',
        'password',
        'status'
    ];

    public function drivers()
    {
        return $this->hasMany(Driver::class);
    }

    public function restaurant_info(){
        return $this->hasOne(RestaurantInfo::class);
    }

    public function restaurant_category(){
        return $this->hasMany(Category::class);
    }


    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function setPasswordAttribute($password){
        if ( !empty($password) ) {
            $this->attributes['password'] = bcrypt($password);
        }
    }
}
