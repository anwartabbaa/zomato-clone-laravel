<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CartItem extends Model
{
    use HasFactory;
    protected $table='cart_items';

    protected $fillable=[
        'user_id',
        'item_id',
        'quantity',
        'restaurant_id',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
    public function item() {
        return $this->belongsTo('App\Models\Item');
    }
}
