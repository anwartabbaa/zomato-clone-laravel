<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    use HasFactory;

    protected $fillable=[
        'quantity',
        'price',
        'item_id',
        'user_id',
        'order_id',

    ];

    public function orders()
    {
        // uncomment me plzzzz
        //  return $this->belongsTo(Order::class, 'order_id', 'id');
    }
    public function itemzz(){
        return $this->belongsTo(Item::class, 'item_id', 'id');
    }
}
