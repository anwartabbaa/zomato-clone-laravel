<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id', 'total_price', 'restaurant_id','address','is_paid',
    ];


    public function item()
    {
        return $this->hasMany(OrderItem::class);
    }
    public function driver_order()
    {
        return $this->belongsTo(DriverOrder::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
