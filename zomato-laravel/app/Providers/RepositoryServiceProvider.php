<?php

namespace App\Providers;

use App\Repository\CategoryRepositoryInterface;
use App\Repository\DriverRepositoryInterface;
use App\Repository\Eloquent\BaseRepository;
use App\Repository\Eloquent\CategoryRepository;
use App\Repository\Eloquent\DriverRepository;
use App\Repository\Eloquent\RestaurantInfoRepository;
use App\Repository\Eloquent\RestaurantRepository;
use App\Repository\EloquentRepositoryInterface;
use App\Repository\RestaurantInfoRepositoryInterface;
use App\Repository\RestaurantRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(EloquentRepositoryInterface::class, BaseRepository::class);
        $this->app->bind(DriverRepositoryInterface::class,DriverRepository::class);
        $this->app->bind(RestaurantInfoRepositoryInterface::class, RestaurantInfoRepository::class);
        $this->app->bind(RestaurantRepositoryInterface::class, RestaurantRepository::class);
        $this->app->bind(CategoryRepositoryInterface::class, CategoryRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

    }
}
